/**
 * 
 */
db.views['person'] = {
	params : {}
	,isinit : false
	,init: function(){
		this.map = UNASSIGNED;
		this.isinit = true;
	}
	,apply: function(work){
		if(!work.person){ return; }
		this.map = work.person;
	}
	,reduce: function(){
		return [];
	}
	,map : null
};
