/**
 * 
 */
db.views['timemap'] = {
	params : {}
	,isinit : false
	,init: function(){
		this.map = {start:null,finish: null,index:null};
		
		this.map.start = moment(this.params.start).clone()
			.startOf('day')
			;
		this.map.finish = moment(this.params.finish).clone()
			.startOf('day')
			.add(1,'day')
			;
		this.map.index = createArray(this.DAYLEN * moment.range(this.map.start,this.map.finish).diff('days'),0);
		
		this.isinit = true;
	}
	,apply: function(work){
		// some of the work items are "TODO". In other words, 
		// there is no time to map
		if(!work.time || !work.time.hours){ return;	}
		
		let $range = moment.range(work.time.start, work.time.finish);
		for(;$range.start < $range.end; $range.start.add('minute',15) ){
			var pos = this.calcPosition($range.start);
			this.at(pos).push(work);
		}
	}
	,reduce: function(){
		return [];
	}
	,map : null
	,MILLITOQUARTERHOUR : (1000 * 60 * 15)
	,MILLITODAY : (1000 * 60 * 60 * 24)
	,DAYLEN : (24*4)
	,INCREMENT: 15
	,calcIncrements:function(period){
		period = moment.range(period);
		$increments = Math.floor((period.finish.getTime()-period.start.getTime())/(this.MILLITOQUARTERHOUR));
		return $days;
	}
	,calcPosition: function(date){
		$day = moment
			.range(this.params.start, date)
			.diff('days')
			;
		$time = date;
		$time = $time.unix()*1000;
		$time = ($time%this.MILLITODAY);
		$time = $time/this.MILLITOQUARTERHOUR;
		$time = Math.floor($time);
		return [$time,$day];
	}
	/**
	 * Used as a mechanism to map our internal 1D 
	 * array to a 2D array that takes into account timezone
	 * 
	 * return an array of work items that map to that index
	 */
	,at:function(pos){
		if(Array.isArray(pos)){
			$onedim = this.from2D(pos);
		}
		else if(!isNaN(Date.parse(pos))){
			
		}
		//log(pos + ' -> ' + $onedim,'now');
		return this.map.index[$onedim];
	}
	,from2D:function(pos){
		$v = (pos[1]*this.DAYLEN) + pos[0];
		return $v;
	}
	,to2D:function(pos){
		return [pos%this.DAYLEN,pos/this.DAYLEN];
	}
	,fromDate:function(pos){
		return null;
	}

};
