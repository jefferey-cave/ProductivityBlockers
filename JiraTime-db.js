
var db = {
	params : {
		start : (function(){$val = new Date();$val.setDate(0);return $val})()
		,finish : (function(){$val = new Date();$val.setDate(0);$val.setMonth($val.getMonth()+1);return $val})()
		,user: 's_jcave'
	}
	,worklog : []
	,AddWork : function (work){
		if(Array.isArray(work)){
			LookupProg.start(work.length);
			for(var $i in work){
				$w = work[$i];
				//this.AddWork($w);
				setTimeout('db.AddWork(' + JSON.stringify($w) + ')');
			}
			return;
		}
		
		LookupProg.finish();
		$work = sanitizeWorkItem(work);
		if($work.person.username != this.params.user){
			return;
		}
		if(!this.params.period){
			this.params.period = moment.range(this.params.start,this.params.finish);
		}
		if ($work.time && !this.params.period.overlaps($work.time.period)){
			return;
		}
		this.worklog.push($work);
		for($v in this.views){
			this.views[$v].apply($work);
		};
		this.Changed();
	}
	,_reduceSummary : null
	,reduceSummary : function(){
		if(this._reduceSummary !== null){
			return this._reduceSummary;
		}
		//NO REDUCE IN INTERNET EXPLORER!!
		//this.worklog.reduce(function(a,b){...});
		var sum = {
			person: null
			,start: null
			,finish: null
			,total: 0
			,daily: 0
			,days: 0
			,util: 0
			,focus: 0
		};
		this.reduceDaily().forEach(function(daily){
			// initialize it the first time
			if(sum.start === null){
				sum.start = daily.date;
				sum.finish = daily.date;
				sum.person = db.views['person'].map;
			};
			
			//min/max date of the period
			if(sum.start > daily.date) sum.start = daily.date;
			if(sum.finish < daily.date) sum.finish = daily.date;
			// if it is not the weekend (days 0 or 6), it is a productive day
			if([0,6].indexOf(daily.date.weekday()) < 0) sum.days++;
			
			sum.total += daily.hours;
			sum.util += daily.util;
			sum.focus += (!isNaN(parseFloat(daily.focus)) && isFinite(daily.focus)) ? daily.focus : 0;
			
		});
		
		sum.util /= sum.days;
		sum.daily = sum.total/sum.days;
		sum.focus /= sum.days;
		
		this._reduceSummary = sum;
		return this._reduceSummary;
		
	}
	,reduceDaily : function(){
		return this.views['daily'].reduce();
	}
	,reduceTicket : function(){
		return this.views['tickets'].reduce();
	}
	,views: {
		'_timemap' : {
			params : {}
			,isinit : false
			,map : null
			,MILLITOQUARTERHOUR : (1000 * 60 * 15)
			,MILLITODAY : (1000 * 60 * 60 * 24)
			,calcDayDiff:function(period){
				period = moment.range(period.start, period.finish);
				$days = period.diff('days');
				return $days;
			}
			,calcIncrements:function(period){
				period = moment.range(period);
				$increments = Math.floor((period.finish.getTime()-period.start.getTime())/(this.MILLITOQUARTERHOUR));
				return $days;
			}
			,calcPosition: function(date){
				$day = this.calcDayDiff({start:this.params.start,finish:date});
				$time = Math.floor((date.getTime()%this.MILLITODAY)/this.MILLITOQUARTERHOUR);
				return [$time,$day];
			}
			,init: function(){
				this.map = createArray(
					(24*4)
					,this.calcDayDiff(this.params)+1
					,0
				);
				this.isinit = true;
			}
			,apply: function(work){
				// some of the work items are "TODO". In other words, 
				// there is no time to map
				if(!work.time){ return;	}
				
				$range = moment.range(work.time.start,work.time.finish);
				for($i = $range.start; $i<$range.finish; $i.setMinutes($i.getMinutes()+15)){
					var pos = this.calcPosition($i);
					this.map[pos[0]][pos[1]].push(work);
				}
			}
		}
	}
	,OnChange : [
		// log the change
		function(self){
			db._reduceDaily = null;
			db._reduceTicket = null;
			db._reduceSummary = null;
		}
		// sort the resulting data
		,function(self){
			self.worklog.sort(function(a,b){
				if(!a.time && !b.time){
					return 0;
				}
				if(!a.time) return -1;
				if(!b.time) return 1;
				
				if(a.time.start < b.time.start) return -1;
				if(a.time.start > b.time.start) return 1;
				return 0;
			});
		}
	]
	,Changed : function(){
		for($c in this.OnChange){
			this.OnChange[$c](this);
		}
	}
	,Clear : function(){
		this.worklog = [];
		for(var v in this.views){
			var view = this.views[v];
			view.params = this.params;
			view.init(true);
		}
		this.Changed();
	}
};

