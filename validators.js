
function Validator(validators){
	for(var arg in arguments){
		for(var v in validators){
			if(validators.hasOwnProperty(v)){
				this[v] = validators[v];
			}
		}
	}
}

Validator.prototype.Add = function(validations){
	for(var msg in validations){
		if(validations[msg]){
			this[msg] = validations[msg];
		}
	}
}

Validator.prototype.isValid = function(value){
	return (0 === this.Validate(value,false).length);
}

Validator.prototype.Validate = function(value, collectAll){
	// default to true
	collectAll = !((collectAll===false) || false);
	var errors = [];

	// loop through all the properties
	for(var msg in this){
		// make sure this is not a prototypical property
		if(this.hasOwnProperty(msg)){
			console.log('Testing: ' + msg);	
			// run the validator
			if(!this[msg](value)){
				errors.push(""+msg);
				if(!collectAll){
					break;
				}
			}
		}
	}
	return errors;
}

Validator.prototype.Assert = function(value, collectAll){
	// default to false
	collectAll = (collectAll===true) || false;
	//get the errors
	var errors = this.Validate(value,collectAll);
	if(errors.length !== 0){
		if(collectAll){
			throw errors;
		}
		else{
			throw errors[0];
		}
	}
	return true;
}

var validDate = new Validator();
validDate['Invalid Date'] = function(strDate){
	var date = Date.parse(strDate);
	return (date && !isNaN(date));	
}
validDate['Invalid Format'] = function(str){
	if(!str){
		return false;
	}
	date = new Date(str);
	str = str
		.replace(/T/g,".")
		.replace(/ /g,".")
		.replace(/:/g,".")
		.replace(/-/g,".")
		.replace(/\//g,".")
		.replace(/\\./g,".")
		.split(".")
		;
	console.log(str + '==' + date)
	return (true
		// && str.length === 5
		&& str[0] == date.getMonth()+1
		&& str[1] == date.getDate()
		&& str[2] == date.getFullYear()
		&& str[3] == date.getHours()
		&& str[4] == date.getMinutes()
	);
}

var validFutureDate = new Validator(validDate);
validFutureDate['Date May not be in past'] = function(str){
	return (Date.now() < new Date(date));
}

var validPastDate = new Validator(validDate);
validPastDate['Date May not be in future'] = function(str){
	return (Date.now() < new Date(date));
}

var validFutureDateFuzzy = new Validator(validDate, {
	'Date may not be in past' : function(str){
		return (Date.now().addMinutes(-30) < new Date(date));
	}
});

/*
//test the validator
var testDates = [
	,{expected:false,value:null}
	,{expected:false,value:''}
	,{expected:false,value:'I am the very model of a modern major general',reason:'Not even sensible'}
	,{expected:false,value:'70/80/2016 20:00'}
	,{expected:false,value:'2017-02-20 20:00'}
	,{expected:false,value:'2017-02-20T20:00.000Z', reason: 'invalid format'}
	,{expected:false,value:'02/29/2017 20:00',reason:'Not a leap year'}
	,{expected:true ,value:'02/29/2020 20:00', reason:'Leap year'}
	,{expected:false,value:'02/20/2015 20:00', reason:'Past'}
]; 
console.log('Testing Dates');
testDates.forEach(function(data){
	var result = validFutureDateFuzzy.isValid(data.value);
	result = data.expected === result ? '/' : 'x';
	console.log('(?) {value}'
		.replace('?',result)
		.replace('{value}',data.value)
	);
});


var validSPID = new Validator({
	'Must be 4 digits' : function (spid){
		spid = spid || '';
		return spid.trim().length == 4;
	}
	,'Must be alpha numeric' : function (spid){
		return spid.toLowerCase() === spid.toLowerCase().replace(/[^0-9a-z]/g,'');
	}
});
//test the validator
var testSPID = [
	,{expected:false,value:null}
	,{expected:false,value:''}
	,{expected:false,value:'I am the very model of a modern major general',reason:'Not even sensible'}
	,{expected:false,value:'12345'}
	,{expected:true,value:'1234'}
	,{expected:false,value:'123'}
	,{expected:false,value:'12'}
	,{expected:false,value:'1'}
	,{expected:false,value:''}
	,{expected:false,value:'!!!!'}
]; 
console.log('Testing SPIDS');
testSPID.forEach(function(data){
	var result = validSPID.isValid(data.value);
	result = data.expected === result ? '/' : 'x';
	console.log('(?) {value}'
		.replace('?',result)
		.replace('{value}',data.value)
	);
});

*/