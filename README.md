# Productivity Blockers

Humans are really bad at working with abstract concepts: calories, velocity, 
money, and most importantly *time*. This is really unfortunate since it is the 
most precious of commodities.

This is a simple report that attempts to create visualizations of time in a 
meaningful manner, helping individuals understand how they are using their 
time, based on data they are probably already required to enter within their 
organization.

It is hoped that this report will act as a means for individuals to communicate 
their needs to their managers more effectively. I will say that again: *this is 
a tool to hold your manager accountable*.

Currently, it only supports JIRA as a data source. There is interest in 
adapters for SDE and Assyst as well.

## Usage

1. Download the files
2. Run the HTA file
3. Fill in your Connection information
4. Run the report

(Only runs on windows)

## Config

Rename the file `config-sample.json` to `config.json`

Values placed in the config file will be used to pre-populate the form

## Interpretation

These reports are designed with the objective of increasing productivity, 
however, while successful, they have proven to introduce new problems. 
Primarily there are things these metrics measure well, and things they don't. 
Quite often people think the output means one thing when in reality it is 
pointing in a totally different direction. 

This is about being brutally honest with yourself. If you are going to lie to 
yourself about how you use your time, go waste it with some other tool.

### Focus Ratio

This is a metric of how many interuptions the person suffered while undertaking 
the task.

Task switching takes time that is completely lost to the actual solving of a 
problem. Low focus ratios are a signal of time wasted to Task Switching.

### Utilization

A measure of how many hours has been applied to a given task, out of the 
expected number of hours. Velocity may be another term that could be used to 
describe this.

An appropriate level of utilization strikes a balance between working toward 
the common objective, and allowing sufficient quiet time for creative processes.

Under-Utilized: leads to boredom
Properly-Utilized: strikes a nice balance between working toward an objective, and creativity
Over-Utilization: creativity is lost, and staff burn out

While there are no hard and fast rules around this, the author feels that 85% 
seems to be a healthy number, leaving 15% of time to for creative efforts.

### Multi-Tasking 

Don't do it. You aren't capable of doing it, and it is a waste of precious time.

The report will flag a block of time stated has having been used twice. This is 
so you can go back to the source and correct your data entry. 

If you require evidence of that:
[The Myth of Multitasking: How "Doing It All" Gets Nothing Done](https://books.google.com/books?hl=en&lr=&id=t_SvTfx-yj0C&oi=fnd&pg=PT70&dq=the+myth+of+multitasking&ots=E8s_WP0Ohq&sig=L4CnlcCt1Rbs_P2F81727yISiQg#v=onepage&q=the%20myth%20of%20multitasking&f=false)

## Data Abuse

Presenting this data to any audience has yet to result in anything short of a 
panic attack in either managers or co-workers.

Frequently made erroneous interpretations:

*  Manager: Do you feel your work is not being recognized?

If an employee has presented this report of their time to you, they do not feel 
their effort is not being recognized, they probably feel like you are getting 
in their way when they try to solve problems. They are asking you to help them 
by removing blockers.

This is why we employ mananagers: to deal with the administrative duties that 
interfere with problem solving. If an employee has brought this report to your 
attention, they are trying to highlight places where you can be more effective 
in your job.

* Staff: I'm a good worker, management doesn't have to keep an eye on me to make sure I'm working

If your manager has brought this to your attention, it is not to make sure you 
are *working*. Rather it is to make sure people aren't interupting you. 

If you are diligent, take a little credit for your work.

* Staff: I'm too busy to bother writing stuff down

See the earlier commments about [Utilization](#Utilization). If you are so busy 
that you do not have time to communicate what you have done to your co-workers, 
you are over-utilized.
