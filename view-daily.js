db.views['daily'] = {
	params : {}
	,isinit : false
	,init: function(force){
		if(!force && this.isinit) return;
		this._map = {};
		
		this._reduce = null;
		this.isinit = true;
	}
	,_map : null
	,_reduce : null
	,apply: function(work){
		//have we initialized? 
		this.init();
		//mark the stashed calc as invalid
		this._reduce = null;
		
		//if we haven't seen this ticket before add it to the index
		if(!work.time || !work.time.hours){ return;	}

		let $key = RenderDate(work.time.start);
		if(!this._map[$key]){
			this._map[$key] = [];
		}
		//add the work item
		this._map[$key].push(work);
	}
	
	/**
	 *
	 */
	,reduce: function(){
		//if we have calculated this before, just send the old results
		if(this._reduce !== null){
			return this._reduce;
		}
		//have we initialized?
		this.init();
		
		// generate a list of every day we are concerned with, which may
		// include days that we don't have work for
		var range=moment.range(this.params.start,this.params.finish);
		let $days = {};
		range.by('days', function(i){
			// create the object, and initialize the sums
			$days[RenderDate(i)] = {
				date:i
				,hours: 0
				,focus: 0
				,util: 0
			};
		});
		
		// loop through the map gathering up every work item
		for(let $workday in this._map){
			// determine the "day" this work item belongs to
			$workday = RenderDate($workday);
			let day = $days[$workday];
			// pull the list of work items for that day
			$workday = this._map[$workday];
			// for each item in the day
			for(let work in $workday){
				work = $workday[work];
				if(work.time){
					day.hours += work.time.hours;
					day.focus += 1;
				}
			}
		}
		
		this._reduce = [];
		for(var d in $days){
			$days[d].focus = 1/$days[d].focus;
			$days[d].util = $days[d].hours/8;
			this._reduce.push($days[d]);
		}
		this._reduce.sort(function(a,b){
			if(a.date < b.date) return -1;
			if(a.date > b.date) return 1;
			return 0;
		});
		
		return this._reduce;
	}

};
