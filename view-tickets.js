db.views['tickets'] = {
	params : {}
	,isinit : false
	,init: function(force){
		if(!force && this.isinit) return;
		this._map = {};
		this.isinit = true;
	}
	,_map : null
	,_reduce : null
	,apply: function(work){
		//have we initialized? 
		this.init();
		//mark the stashed calc as invalid
		this._reduce = null;
		
		//if we haven't seen this ticket before add it to the index
		if(!this._map[work.ticket.key]){
			this._map[work.ticket.key] = [];
		}
		//add the work item
		this._map[work.ticket.key].push(work);
	}
	,reduce: function(){
		//have we initialized?
		this.init();
		//if we have calculated this before, just send the old results
		if(this._reduce !== null){
			return this._reduce;
		}
		
		var reduce = [];
		for($t in this._map){
			$ticket = {
					ticket: ''
					,opened: null
					,status: null
					,title: null
					,hours:0
					,focus:0
				};
			this._map[$t].forEach(function(work){
				$ticket.ticket = work.ticket.key;
				$ticket.title = '';
				if(work.time){
					$ticket.hours += work.time.hours;
					$ticket.focus += 1;
				}
			});
			$ticket.focus = 1/$ticket.focus;
			$ticket.util = $ticket.hours/8;
			reduce.push($ticket);
		}
		reduce.sort(function(a,b){
			if(a.hours > b.hours) return -1;
			if(a.hours < b.hours) return 1;
			return 0;
		});
		
		this._reduce = reduce;
		return reduce;
	}

};
