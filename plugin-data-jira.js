var jira = {
	getJql:function (params){
		var authtoken = config.jira.cookie;
		if(!authtoken){
			log("No Auth Token");
			return;
		}
		var url =[
				getJiraUrl(),
				"/rest/api/2/search?startIndex=0&fields=worklog,assignee,status,key,summary&maxResults=1000&jql=",
				encodeURI(params.jql)
			].join('');
		log(url);
		$.ajax({
			url: url,
			type: "GET",
			dataType: "json",
			contentType: "application/json",
			async: true,
			//timeout: 100000,
			beforeSend: function(xhr){
				xhr.setRequestHeader('Authorization','Basic '+authtoken);
			},
			success: function (issuedata) {
				params.callback(issuedata);
			},
			error: function(response,textStatus,error) {
				if('AUTHENTICATION_DENIED' == response.getResponseHeader('X-Seraph-LoginReason')){
					throw("You appear to have been flagged for CAPTCA. Please contact the service desk. ");
				}
				else if(textStatus == 'timeout'){
					throw("JQL query timed out");
				}
				else{
					throw(error + '(' + textStatus + '):\n ' + JSON.stringify(response,null,4));
				}
			}
		});
	}
	,retrieve: function(range){
		var $fromDate = range.start;
		var $toDate = range.finish;
		
		this.getJql({
				jql:'updated < '+ $toDate +' and updated > ' + $fromDate
				,callback:function(resp){
					LookupProg.start(resp.issues.length);
					resp.issues.forEach(function(issue) {
						db.AddWork(jira.mapTicketToWork(issue));
						LookupProg.finish();
					});
				}
			});
	},
	
	
	/**
	 * Given a ticket, it returns a list of all work that should have been and has been done.
	 * 
	 * @return Array of work
	 */
	mapTicketToWork : function(ticket){
		$AVATARSIZE = ['48x48','32x32'][0];
		$emit = [];
		$tick = {
				'key' : ticket.key
				,'id' : ticket.id
				,'title' : ticket.fields.summary
			};
		
		$emit.push({
			'id' : ticket.id
			,'person' : (!ticket.fields.assignee)
				? UNASSIGNED
				: {
					'username' : ticket.fields.assignee.name
					,'name' : ticket.fields.assignee.displayName
					,'email' : ticket.fields.assignee.emailAddress
					,'avatar' : ticket.fields.assignee.avatarUrls[$AVATARSIZE]
				}
			,'time' : null
			,'ticket' : $tick
			,'comment' : ticket.fields.description
		});
		
		if(ticket.fields.worklog.worklogs){
			if(ticket.fields.worklog.worklogs.length > 0){
				$emit = [];
			}
			for(w in ticket.fields.worklog.worklogs){
				$work = ticket.fields.worklog.worklogs[w];
				$work = {
					'id' : $work.id
					,'person' : {
							'username' : $work.author.name
							,'name' : $work.author.displayName
							,'email' : $work.author.emailAddress
							,'avatar' : (function(){
									try{ return $work.author.avatarUrls[$AVATARSIZE]; }
									catch(e){ return UNASSIGNED.avatar; }
								})()
						}
					,'time' : {
						'start' : moment.utc($work.started)
						,'hours' : ($work.timeSpentSeconds/3600)
						}
					,'ticket' : $tick
					,'comment' : $work.comment
				};
				$emit.push($work);
			}
		}
		
		// run everything through the wash
//		for(w in $emit){
//			$emit[w] = sanitizeWorkItem($emit[w]);
//		}
		// send it back
		return $emit;
	}
};